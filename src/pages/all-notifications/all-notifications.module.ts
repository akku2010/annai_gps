import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllNotificationsPage } from './all-notifications';
import { SelectSearchableModule } from '../../../node_modules/ionic-select-searchable';

@NgModule({
  declarations: [
    AllNotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllNotificationsPage),
    SelectSearchableModule
  ],
})
export class AllNotificationsPageModule {}
