import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteVoilationsPage } from './route-voilations';
import { SelectSearchableModule } from 'ionic-select-searchable';
@NgModule({
  declarations: [
    RouteVoilationsPage,
  ],
  imports: [
    IonicPageModule.forChild(RouteVoilationsPage),
    SelectSearchableModule
  ],
})
export class RouteVoilationsPageModule {}
