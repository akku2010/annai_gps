import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverSpeedRepoPage } from './over-speed-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    OverSpeedRepoPage,
  ],
  imports: [
    IonicPageModule.forChild(OverSpeedRepoPage),
    SelectSearchableModule
  ],
})
export class OverSpeedRepoPageModule {}
