import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  isChecked: boolean;
  notif: string;
  isBooleanforLive: boolean;
  isBooleanforDash: boolean;
  constructor(
    public events: Events,
    private tts: TextToSpeech,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController) {

    if (localStorage.getItem("notifValue") != null) {
      this.notif = localStorage.getItem("notifValue");
      if (this.notif == 'true') {
        this.isChecked = true;
      } else {
        this.isChecked = false;
      }
    }
  }
  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") == 'live') {
        this.isBooleanforLive = true;
        this.isBooleanforDash = false;
      } else {
        if (localStorage.getItem("SCREEN") == 'dashboard') {
          this.isBooleanforDash = true;
          this.isBooleanforLive = false;
        }
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  setNotif(notif) {
    console.log(notif);
    this.events.publish('notif:updated', notif);
    this.isChecked = notif;
    if (notif === true) {
      this.tts.speak('You have succesfully enabled voice notifications')
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
    }
  }

  rootpage() {
    // let alert = this.alertCtrl.create({
    //   title: 'Alert',
    //   subTitle: 'Choose default screen',

    // })
    let alert = this.alertCtrl.create();
    // alert.setTitle('Alert');
    alert.setSubTitle('Choose default screen');
    alert.addInput({
      type: 'radio',
      label: 'Dashboard',
      value: 'dashboard',
      checked: this.isBooleanforDash
    });

    alert.addInput({
      type: 'radio',
      label: 'Live Tracking',
      value: 'live',
      checked: this.isBooleanforLive
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data)
        localStorage.setItem("SCREEN", data);
        const toast = this.toastCtrl.create({
          message: 'Default page set to ' + data + ' page',
          duration: 2000
        });
        toast.present();
      }
    });
    alert.present();
  }

}
